'use strict';

var app = angular.module('mifos-payment.browserStorageService', []);

app.factory('cookieStorage', function ($cookies, $timeout) {
    var expired = new Date();
    expired.setTime(expired.getTime() + (60 * 1000));
    return {
        set: function set(dataKey, data) {
            $cookies.putObject(dataKey, data);
        },
        get: function get(dataKey) {
            return $cookies.getObject(dataKey);
        },
        remove: function remove(dataKey) {
            return $cookies.remove(dataKey);
        }
    }

});