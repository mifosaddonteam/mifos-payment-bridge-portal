'use strict';

var configApp = angular.module('mifos-payment.config', []);

configApp.factory('paymentConfigService', function(){
    var configs ={
        'backend': 'http://localhost:4848/api/v1',
        'version': 1.0
    };
    return configs;
});

