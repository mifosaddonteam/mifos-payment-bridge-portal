'use strict';

angular.module('myApp.view1', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/payments', {
            templateUrl: 'payment/view-payment.html',
            controller: 'PaymentsController'
        }).when('/payment-details/:id', {
            templateUrl: 'payment/payment-details.html',
            controller: 'PaymentDetailsController',
            resolve: {
                'isNew': function () {
                    return false;
                }
            }
        });
    }])

    .controller('PaymentsController', ['$scope', '$location', '$http', '$log', 'paymentConfigService',
        function ($scope, $location, $http, $log, paymentConfigService) {
            $scope.payments = [];
            $scope.isErr = false;
            $scope.errorMessage = '';

            $scope.getPayments = function () {
                $http({
                    method: 'GET',
                    url: paymentConfigService.backend + '/payment',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    $scope.payments = response.data;
                    $scope.isErr = false;
                    $scope.errorMessage = 'Fetched payments successfully!';
                    $log.info($scope.errorMessage);
                }, function (response) {
                    $scope.errorMessage = response.data.message;
                    $scope.isErr = true;
                    $log.error(response.data);
                });
            };


            $scope.getPayments();
        }])

    .controller('PaymentDetailsController', ['$scope', '$location', '$http', '$log', 'paymentConfigService', '$routeParams', 'isNew',
        function ($scope, $location, $http, $log, paymentConfigService, $routeParams, isNew) {
            $scope.payment = {};
            $scope.isErr = false;
            $scope.isNew = isNew;
            $scope.errorMessage = '';
            $scope.payment.active = true;

            $scope.getPayment = function () {
                var paymentId = $routeParams.id;
                $http({
                    method: 'GET',
                    url: paymentConfigService.backend + '/payment/' + paymentId,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    $scope.payment = response.data;
                    $scope.isErr = false;
                    $log.info($scope.payment);
                }, function (response) {
                    $scope.errorMessage = response.data.message;
                    $scope.isErr = true;
                    $log.error(response.data);
                });
            };

            if (!$scope.isNew) {
                $scope.getPayment();
            }
        }])