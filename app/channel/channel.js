'use strict';

angular.module('mifos-payment.channel', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/channels', {
            templateUrl: 'channel/view-channels.html',
            controller: 'ChannelsController'
        }).when('/channel-details', {
            templateUrl: 'channel/channel-details.html',
            controller: 'ChannelDetailsController',
            resolve: {
                'isNew': function () {
                    return true;
                }
            }
        }).when('/channel-details/:id', {
            templateUrl: 'channel/channel-details.html',
            controller: 'ChannelDetailsController',
            resolve: {
                'isNew': function () {
                    return false;
                }
            }
        });
    }])

    .controller('ChannelDetailsController', ['$scope', '$location', '$http', '$log', 'paymentConfigService', '$routeParams', 'isNew',
        function ($scope, $location, $http, $log, paymentConfigService, $routeParams, isNew) {
            $scope.channel = {};
            $scope.isErr = false;
            $scope.isNew = isNew;
            $scope.errorMessage = '';
            $scope.channel.active = true;

            $scope.createChannel = function () {
                $http({
                    method: 'POST',
                    data: $scope.channel,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    url: paymentConfigService.backend + '/channel'
                }).then(function successCallback(response) {
                    $scope.errorMessage = 'Saved channel successfully!';
                    $log.info($scope.errorMessage);
                    $location.path('/channels');
                }, function errorCallback(response) {
                    $scope.errorMessage = response.data.message;
                    $scope.isErr = true;
                    $log.error(response.data);
                });
            };

            $scope.getChannel = function () {
                var channelId = $routeParams.id;
                $http({
                    method: 'GET',
                    url: paymentConfigService.backend + '/channel/' + channelId,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    $scope.channel = response.data;
                    if ($scope.channel.channel_type == 'MOBILE_MONEY_CHANNEL') {
                        $scope.channel.channel_type = '1';
                    } else if ($scope.channel.channel_type == 'BANKING_CHANNEL') {
                        $scope.channel.channel_type = '2';
                    } else if ($scope.channel.channel_type == 'EMAIL_MONEY_CHANNEL') {
                        $scope.channel.channel_type = '3';
                    }
                    $scope.isErr = false;
                    $log.info( $scope.channel);
                }, function (response) {
                    $scope.errorMessage = response.data.message;
                    $scope.isErr = true;
                    $log.error(response.data);
                });
            };

            if (!$scope.isNew) {
                $scope.getChannel();
            }
        }])

    .controller('ChannelsController', ['$scope', '$location', '$http', '$log', 'paymentConfigService',
        function ($scope, $location, $http, $log, paymentConfigService) {
            $scope.channels = [];
            $scope.isErr = false;
            $scope.errorMessage = '';

            $scope.getChannels = function () {
                $http({
                    method: 'GET',
                    url: paymentConfigService.backend + '/channel',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    $scope.channels = response.data;
                    $scope.isErr = false;
                    $scope.errorMessage = 'Fetched channels successfully!';
                    $log.info($scope.errorMessage);
                }, function (response) {
                    $scope.errorMessage = response.data.message;
                    $scope.isErr = true;
                    $log.error(response.data);
                });
            };


            $scope.getChannels();

        }]);