# mifos-payment-bridge-portal - Portal for managing mifos payment bridge

This project is to help individuals using mifos easily integrate to external mobile wallets and banks for external payments.

See:
1. [Mifos Payment Bridge] (https://omexic@bitbucket.org/mifosaddonteam/mifos-payment-bridge.git)
2. [Beyonic Handler] (https://omexic@bitbucket.org/mifosaddonteam/beyonic-handler.git)


## Contact

For more information `aomeri@omexit.com`
